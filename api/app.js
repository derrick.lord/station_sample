'use strict';

const express = require('express');
const http = require('http');
const helmet = require('helmet');
const cors = require('cors');
const axios = require('axios');

const userController = require('./users/user.controller');
const {catchErrors, notFound} = require('./handlers/errorHandler');


//Server setup
const app = express();
app.server = http.createServer(app);


//Protect against Cross Origin attacks + set HTTP response headers
app.use(cors());
app.use(helmet());

//TODO: move GET route to seperate module
//TODO: add error handling helper module
app.get('/users', catchErrors(userController.requestUsers));
app.use(notFound);


module.exports = app;




import React from 'react';
import userImage from '../images/male.png';
import '../css/Card.css';

export default class Card extends React.Component{
	constructor(){
		super();
		this.state = {
			showAddress: false
		}
	}

	toggleAddress = ()=>{
		this.setState({showAddress: !this.state.showAddress});
	}

	displayContent = ()=>{
		if(this.state.showAddress){
			return (
				<p className="card-text">
					<span className="card-label card-phone">Phone:</span> {this.props.phone}<br/>
					<span className="card-label">Street:</span> {this.props.address.street} <br/>
					<span className="card-label">Suite:</span> {this.props.address.suite}<br/>
					<span className="card-label">City:</span> {this.props.address.city} <br/>
					<span className="card-label">Postal Code:</span> {this.props.address.zipcode}
				</p>
				
			)
		}else{
			return <p className="card-text"></p>
		}
	}

	render(){
		return(
			<div className="card" key={this.props.idx}>
				<img src={userImage} className="card-img-top" alt="logo" />
				<div className="card-body">
					<button href="/#" onClick={this.toggleAddress} className="btn btn-danger">{this.props.name}</button>
					{this.displayContent()}
				</div>
			</div>
		)
	}
}

import React from 'react';
import stationslogo from './images/stationslogo.png';
import 'bootstrap/dist/css/bootstrap.css';
import './css/App.css';
import Container from './components/Container';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={stationslogo} className="App-logo" alt="logo" />
      </header>
      <Container className="container"/>
    </div>
  );
}

export default App;

import React from 'react';
import axios from 'axios';
import Card from './Card';
const apiURL = 'http://localhost:8080/users';


export default class Component extends React.Component{
	constructor(){
		super();
		this.state = { users: []};
	}

	async componentDidMount(){
		try{
			const response = await axios.get(apiURL);
			const users = response.data.users;
			this.setState({users});
		} catch(error){
			console.log(error);
		}
	}

	showUsers(){
		if(this.state.users.length >0){
			return this.state.users.map((user, idx)=> <Card key={idx} {...user}/>)
		}else{
			return <h1>No Connection to API...</h1>
		}
	}

	render(){
		return(
			<div className="col-md-12">
				<div className="row">
					{this.showUsers()}
				</div>
			</div>
		)
	}
}


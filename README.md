# Station Code Sample App

This a simple app with a React frontend and Express backend API

## Getting Started

1. Clone or Download the Repo
2. cd /api
3. Run "npm install" at the command prompt
4. cd ../ui
5. Run "npm install" at the command prompt (or "yarn install")


## Running API

1. Run "npm start" at the command line
2. Access the api via the following url

	http://localhost:8080/users - for user list

## Running UI

1. Run "npm start" at the command prompt
2. Access the ui via the following url

	http://localhost:3000 - for Frontend interface

note: UI port may be set to value of process.env.PORT (if variable is set)


## Running with Docker

1. From the station_sample root folder:
	run "docker-compose up -d" - run docker compose (detached console)
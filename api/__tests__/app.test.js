const request = require('supertest');
const app = require('../app');


describe('Test the users API endpoint', ()=>{
	test('It should respond to the GET request with 200 status', async ()=>{
		const response = await request(app).get('/users');
		expect(response.statusCode).toBe(200);
	});
	
	test('It should respond with array with length greater than 0 items', async ()=>{
		const response = await request(app).get('/users');
		expect(response.body.users.length).toBeGreaterThan(0);
	});

});
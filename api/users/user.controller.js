const axios = require('axios');

exports.requestUsers = async (req, res)=>{
	let response = await axios.get('https://jsonplaceholder.typicode.com/users')
	let userData = response.data.map((user)=>{
		return {
			name: user.name,
			phone: user.phone,
			address: {
				street: user.address.street,
				suite: user.address.suite,
				city: user.address.city,
				zipcode: user.address.zipcode
			}
		}
	});
	res.json({users: userData});
}

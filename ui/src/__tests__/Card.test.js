import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {mount, shallow, render} from 'enzyme';

import Card from '../components/Card';

Enzyme.configure({ adapter: new Adapter() });

const user = {
	"name": "Leanne Graham",
	"phone": "1-770-736-8031 x56442",
	"address": {
		"street": "Kulas Light",
		"suite": "Apt. 556",
		"city": "Gwenborough",
		"zipcode": "92998-3874"
	}
}

describe('Card Button Click: should toggle address visibility', () => {
	it('should render properly with props', ()=>{
		const card = shallow(<Card  {...user} key={9}/>);
		expect(card).toMatchSnapshot();
	});

	it('should change showAddress state when clicked', ()=>{
		const card = shallow(<Card  {...user} key={9}/>);
		card.find('.btn').simulate('click');
		expect(card.state().showAddress).toBe(true);
	});
});

